verbose = 0 # Muestra todos los pasos de todos los metodos
verbose2 = 0 # Solo muestra resultados medianamente importantes (como los resultados de las sumas, por ejemplo)

# quitZeros quita los "leading zeroes" de un numero en forma de lista.
# in -> list
# out -> list
def quitZeros(n1):
    while n1[0] == '0':
        n1.remove('0')
    return n1

# Algoritmo custom para sumar paso a paso, comprobando que todos los digitos son impares.
# in -> list, list
# out -> int (si ha sido correctamente sumado (0) o no(1))
def customSum(n1, n2):
    if verbose2:
        print "customSum: \tn1 =", n1, "\n\t\tn2 =", n2

    carry = 0 # El carry de la suma
    aux = 0 # Numero auxiliar en el que guardaremos la suma parcial
    

    for i in range(0, len(n1)):
       
        a1 = int(n1[len(n1) - i - 1])
        a2 = int(n2[len(n1) - i - 1])
        
        if carry == 0:
            aux = a1 + a2
        elif carry == 1:
            aux = a1 + a2 + 1
            carry = 0
        
        if aux / 10 == 1:
            carry = 1
        
        if (aux % 10) % 2 != 1:
            if verbose2:
                print "DIGITO PAR ENCONTRADO"
            if verbose2:
                print "i:", i, "\taux =", aux, "\tcarry =", carry
            return 1
        
        if verbose:
            print "i:", i, "\taux =", aux, "\tcarry =", carry
        aux = 0
 
    return 0
        
cont = 0
for i in xrange(1, 10**9 + 1):
    n1 = i
    n1 = str(n1)
    n1 = list(n1)
    n2 = list(n1)
    n2.reverse()
    n2 = quitZeros(n2)
    
    n2int = int(''.join(map(str,n2)))    
    
    if i > n2int:
        continue
        
    if(len(n2) == len(n1)):
        if(customSum(n1, n2) == 0):
            cont = cont + 2
        
        if verbose:
            print " n1:", n1, "\n n2:", n2
            
print "Cont final:", cont
