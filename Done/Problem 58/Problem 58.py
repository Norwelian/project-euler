import numpy as np
from itertools import count, islice
from math import sqrt
import sys

def isPrime(n):
    if n < 2: return False
    return all(n%i for i in islice(count(2), int(sqrt(n)-1)))

def iteration(center, size, paso, cont, maxsize, primos):
    x = center - size
    y = center + size

    if(paso < maxsize):        
        x = x + paso
        cont = cont + paso
        if(isPrime(cont)):
            primos = primos + 1
        
        y = y - paso
        cont = cont + paso
        if(isPrime(cont)):
            primos = primos + 1
        
        paso = paso + 1
        
        x = x - paso
        cont = cont + paso
        if(isPrime(cont)):
            primos = primos + 1
        
        y = y + paso
        cont = cont + paso
        if(isPrime(cont)):
            primos = primos + 1
        
        paso = paso + 1
        
    else: 
        x = x + paso - 1
        cont = cont + paso - 1
        if(isPrime(cont)):
            primos = primos + 1
    
    return [paso, cont, primos]
        

def fillMatrix(center, paso, cont, maxsize, iterations, primos):
    pasoAux = paso
    contAux = cont
    primosAux = primos
    for i in range(0, iterations):
        [pasoAux, contAux, primosAux] = iteration(center, i, pasoAux, contAux, size, primosAux)
        tamDiag = (i+1) * 4 + 1
        porcentaje = int(np.floor((float(primosAux) / float(tamDiag) ) * 100))
        if porcentaje < 10:
            print i*2 + 3, porcentaje
            break
        #print primosAux, tamDiag, 
    return porcentaje

        
size = 200010
center = size/2
iterations = size/2 + 1
cont = 1
paso = 1

primos = -1

#matrix = np.zeros(shape=(size,size), dtype=int)
d1=[]
d2=[]

#matrix[center][center] = 1

porc = fillMatrix(center, paso, cont, size, iterations, primos)
