f = open('poker.txt', 'r')

debug = 1

RoyalFlush = 1
StraightFlush = 2
FourOfKind = 3
Full = 4
Flush = 5
Straight = 6
ThreeOfKind = 7
TwoPairs = 8
Pair = 9
HighCard = 10

def getValor(elem):
    if elem == 'T':
        valor = 10
    elif elem == 'J':
        valor = 11
    elif elem == 'Q':
        valor = 12
    elif elem == 'K':
        valor = 13
    elif elem == 'A':
        valor = 14
    else:
        valor = int(elem)
    return valor

def getValorMaximo(listaValores):
    return max(listaValores)

def parseValores(lista):
    return [getValor(lista[0]),getValor(lista[1]),getValor(lista[2]),getValor(lista[3]),getValor(lista[4])]

def getKind(v, c):
    ret = 11
    retVal = 0
    i = v[0]
    v.sort()
    if(c[0] == c[1] == c[2] == c[3] == c[4]):    #Color igual
        if(v[0] == 10 and v[1] == 11 and v[2] == 12 and v[3] == 13 and v[4] == 14):
            if ret > RoyalFlush:
                ret = RoyalFlush
                retVal = 0
        elif(v[1] == (i+1) and v[2] == (i+2) and v[3] == (i+3) and v[4] == (i+4)):
            if ret > StraightFlush:
                ret = StraightFlush
                retVal = v[4]
        else:
            if ret > Flush:
                ret = Flush
                retVal = getValorMaximo(v)
       #Color diferente
    else: 
        if(v[0] == v[1] == v[2] == v[3] or v[1] == v[2] == v[3] == v[4]):
            if ret > FourOfKind:
                ret = FourOfKind
                retVal = v[1]
        elif((v[0] == v[1] == v[2] and v[3] == v[4]) or(v[0] == v[1] and v[2] == v[3] == v[4])):
            if ret > Full:
                ret = Full
                retVal = v[2]
        elif(v[1] == i+1 and v[2] == i+2 and v[3] == i+3 and v[4] == i+4):
            if ret > Straight:
                ret = Straight
                retVal = v[4]
        elif(v[0] == v[1] == v[2] or v[1] == v[2] == v[3] or v[2] == v[3] == v[4]):
            if ret > ThreeOfKind:
                ret = ThreeOfKind
                retVal = v[2]
        elif((v[0] == v[1] and v[2] == v[3]) or (v[0] == v[1] and v[3] == v[4]) or (v[1] == v[2] and v[3] == v[4])):
            if ret > TwoPairs:
                ret = TwoPairs
                retVal = v[3]
        elif((v[0] == v[1]) or (v[1] == v[2])):
            if ret > Pair:
                ret = Pair
                retVal = v[1]
        elif((v[2] == v[3]) or (v[3] == v[4])):
            if ret > Pair:
                ret = Pair
                retVal = v[3]
        else:
            if ret > HighCard:
                ret = HighCard
                retVal = getValorMaximo(v)
    return [ret, retVal]
    
cont = 0    
    
for line in f:
    aux = line.split()
    [a1,b1,c1,d1,e1] = [aux[0],aux[1],aux[2],aux[3],aux[4]]
    [a2,b2,c2,d2,e2] = [aux[5],aux[6],aux[7],aux[8],aux[9]]
    
    if debug:
        print "P1:\t", a1, b1, c1, d1, e1
        print "P1 val:\t", getValor(a1[0]),getValor(b1[0]),getValor(c1[0]),getValor(d1[0]),getValor(e1[0])
        print "P2:\t", a2, b2, c2, d2, e2
        print "P2 val:\t", getValor(a2[0]),getValor(b2[0]),getValor(c2[0]),getValor(d2[0]),getValor(e2[0])
        
    valores1 = [a1[0], b1[0], c1[0], d1[0], e1[0]]
    colores1 = [a1[1], b1[1], c1[1], d1[1], e1[1]]
    valores1 = parseValores(valores1)
    max1 = getValorMaximo(valores1)
    kind1, kindVal1 = getKind(valores1, colores1)
    
    valores2 = [a2[0], b2[0], c2[0], d2[0], e2[0]]
    colores2 = [a2[1], b2[1], c2[1], d2[1], e2[1]]
    valores2 = parseValores(valores2)
    max2 = getValorMaximo(valores2)
    kind2, kindVal2 = getKind(valores2, colores2)
    
    if debug:
        print valores1, kind1, kindVal1
        print valores2, kind2, kindVal2
        
    win = 0
    
    if(kind1 > kind2): #Gana el 2
        win = 2
    elif(kind1 < kind2): #Gana el 1
        win = 1
    else:
        if(kindVal1 < kindVal2): #Gana el 2
            win = 2
        elif(kindVal1 > kindVal2): #Gana el 1
            win = 1
        else: #Parejas iguales, por ejemplo
            while(max1 == kindVal1):
                valores1.remove(kindVal1)
                max1 = getValorMaximo(valores1)   
            
            while(max2 == kindVal2): 
                valores2.remove(kindVal2)
                max2 = getValorMaximo(valores2)
                
            if(max1 < max2): #Gana el 2
                win = 2
                if debug:
                    print "ay oopos, gana el dos por puntos", max1, max2
            elif(max1 > max2): #Gana el 1
                win = 1
                if debug:
                    print "ay oopos, gana el uno por puntos", max1, max2

            else: #Empate
                print "cant be!"
    
    if(win == 1): #Gano el 1!
        cont = cont + 1
        if debug:
            print "gana 1"
    
    if debug:
        print "\n"
            
print cont
