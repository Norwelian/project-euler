import itertools as itr

def gimme_numbers(index):
    n_list = 123432
    n_list = list(str(n_list))
    return int(n_list[index])
    
def nthn(index, number):
    a = 0
    cont = 0
    accum = 0
    for i in itr.count(index):
        aux = gimme_numbers(i % 6)
        a = a + aux % 123454321
        a = a % 123454321
        accum = (accum * 10) % 123454321 + aux % 123454321
        accum = accum % 123454321
        cont = cont + 1        
        
        #print "aft a:", a, "accum:", accum, "namber:", aux, "quiero:", number, "cont:", cont
        #raw_input("_")
                
        if(a == number % 123454321):
            break
    return [cont, accum]
    
def do_it_til_n(n):
    cont = 0
    accum = 0
    auxaccum = 0
    auxcont = 0
    for i in itr.count(1):
        [auxcont, auxaccum] = nthn(cont, i)
        cont = cont + auxcont % 6
        cont = cont % 6
        accum = accum + auxaccum % 123454321
        accum = accum % 123454321
        
        if(i % (n / 100.) == 0.0) :
            print ((100*i) / n), "%"
        
        if(i == (n)):
            break
    
    return accum
